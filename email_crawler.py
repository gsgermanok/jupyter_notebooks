import requests, re
from bs4 import BeautifulSoup

def create_buffer(line, filename):
    buff =  open(filename, "a+")
    buff.write(line + "\n")
    buff.close()

def is_in_buffer(txt, filename):
    with open(filename) as search:
        for line in search:
            line = line.strip()
            if txt == line:
                return True
            
def get_emails(mystr):

    emails = open("emails.csv", "a+")
    email_list = re.findall("([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)", mystr) 

    for email in email_list:
        if not is_in_buffer(email, "email_buffer.log"):
            emails.write(email + "\n")
            create_buffer(email, "email_buffer.log")

    emails.close()
    
def get_urls(base):
    global exclude
    exclude = (
    'http',
    'https',
    'https://www.facebook.com/',
    'https://www.twitter.com',
    'mailto:',
    'tel:',
    '#',
    'www',
    'javascript:history.back()',
    'javascript:void(0)',
    'javascript',
    '/None',
    '.jpg',
    '.JPG',
    '.PNG',
    '.png',
    '.webp',
    '.WEBP',
    '.jpeg',
    '.JPEG',
    '.GIF',
    '.PDF',
    '.pdf',
    '.gif'
    )
 
    response = requests.get(base)
    soup = BeautifulSoup(response.text, 'html.parser')
    soup_str = str(soup)
    urls = open("urls.csv", "a+")
    for link in soup.find_all('a'):
            
        path = str(link.get('href'))
        
        if(not any(x in path for x in exclude) and not is_in_buffer(path, "buffer.log")):
            if path != base:
                if path.startswith("/"):
                    create_buffer(path, "buffer.log")
                    urls.write(url + path + "\n")
                    print(url + path)
                    get_emails(soup_str)
                    get_urls(url + path)
                else:
                    create_buffer("/" + path, "buffer.log")
                    urls.write(url + "/" + path + "\n")
                    print(url + "/" + path)
                    get_emails(soup_str)
                    get_urls(url + "/" + path)
            
    urls.close()
    
urls = open("urls.csv", "r+")
urls.truncate(0)
urls.close()
emails = open("emails.csv", "r+")
emails.truncate(0)
emails.close()
buff =  open("buffer.log", "r+")
buff.truncate(0)
buff.close()
buff =  open("email_buffer.log", "r+")
buff.truncate(0)
buff.close()
url = input("URL: ") # 'http://entrerios.tur.ar'
get_urls(url)
print("exit")